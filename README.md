# dev-driver

CSCI 401 Operating Systems
Instructor: Gedare Bloom
Final Project
Members:
	The members of the team are Rajjwal Rawal, Utsab Khakurel and Pratyush Thapa.
	Our group number is 9.

Project Choice and reasons behind the choice:
	After some discussion we decided that we were interested in the device driver or the file system project. However, after further talk and thinking about constraints such as timeframe and collective resources on the class, we decided to go with the Device Driver project. This would mean that we would have better chances of succeeding in the project since most teams are doing the same project(from show of hands in class) and hence will have collective help of peers.

Goals:
	The ultimate goal of this project, including all the options, is to familiarize us to the linux kernel. More importantly, this hands on project will have our hands dirty with making modifications to the linux kernel.

Objective of the Device Driver Project:
	Our main objective is to implement a pseudo-device driver for a mailbox object. A mailbox has a read end and a write end which makes is similar to a persistent pipe. Processes have the ability to open either end, then read or write to it and close it when they are done.

Breaking the project down(Implementation):
- Initialize by creating a basic module that can load and unload
- Start by creating a module that only allows reading and initialize your module with
fixed string of bytes in the init function and then adding write functionality.
- Locking and error handling can then deal with the remaining cases.
- Use some kernel locks and wait queues in the driver so that module would
not be able to be unloaded until all user processes have closed the mailbox.
- Additional features will then be added. It may be the allowance of query for how many bytes of data are available, supporting partial writes and reads,
allowing multiple writers or readers to open the mailbox


Testing:
Our team intends on testing the project after the implementation is done. We intend to do so by writing userspace testing programs that will open/close/read/write to your driver all at the same time, or in series. These tests will verify the correctness of your driver. They will also provide some benchmarking of your driver to determine the overhead of doing a write, how does it scale with large writes and reads (many KB to MB), what is the latency to ‘ping-pong’ a write to a reader and get a return write on a separate mailbox.


Timeline of the project:

- Monday,​ ​November​ ​27:
	Initial meet and start by creating a repo
- Saturday,​ ​December​ ​2:​ ​
Create the basic module
- Monday,​ ​December​ ​4:​ ​
Make modifications to the module allowing reading and 
initializing the module.
- Tuesday,​ ​December​ ​5:​ ​
Add Testing and additional features after finishing locking and 
error handling
- Wednesday,​ ​December​ ​6:​ ​
Project Submission Day


We need to note that this is the intended timeline, however, due to finals and some of the teammates traveling abroad, we are finding difficulties in finding time to collaborate. Hence the timeline is expected to be pushed back by a couple of days.


##BACKLOG


##References:

Code that you see in this repository has been influenced by these resources. Code has not been explicitly copied. However snippets of code might look similar.

http://derekmolloy.ie/writing-a-linux-kernel-module-part-2-a-character-device/

https://appusajeev.wordpress.com/2011/06/18/writing-a-linux-character-device-driver/
    This blog made clear of how to run the tests, how they work, and how every method plays in. It's well written.
