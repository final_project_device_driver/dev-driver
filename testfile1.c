#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#define BUFFER_LENGTH 20
static char receive[BUFFER_LENGTH];

int main() {
    int dum, fs;
    char stringToSend[BUFFER_LENGTH];

    fs = open("/dev/ebbchar", O_RDWR);

    while(fs>0){
        printf("Send a string for the kernel module:\n");
        scanf("%[^\n]%*c", stringToSend);
        printf("Trying to write the file to the mailbox[%s].\n", stringToSend);
        dum = write(fs, stringToSend, strlen(stringToSend));
        
        if (dum < 0) {
            perror("Failed to write the message to the device.");
            return errno;
        }
        getchar();
        
        printf("Reading from the device..\n");
        dum = read(fs, receive, BUFFER_LENGTH);
        if (dum < 0) {
            perror("Cannot read the message from the device.\n");
            return errno;
        }
        printf("The message is: [%s]\n", receive);
    }

    perror("Open device failed.");

    printf("End of the program\n");
    return 0;
}
